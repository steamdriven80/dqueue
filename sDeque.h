#ifndef _SDEQUE_H_
#define _SDEQUE_H_

/***
  * deferredm
  *
  * this is our memory management object that can wrap raw memory pointers with
  * a reference counter.  this lets our code freely pass the memory around without
  * worrying about a reference-after-free error
  *
  **/

#ifndef NULL
#define NULL 0L
#endif

template<class T>
class deferred {
protected:

// our managed memory
T*    m_managed;

// object reference counter
int   m_refcount;

// internal: deletes the managed memory when there are no more refernces to it
void release() { if(!m_refcount) throw("attempted to free invalid memory object in defer"); delete m_managed; m_managed = NULL; }

public:

// initialize with a raw pointer
deferred(T* obj): m_managed(obj), m_refcount(0) { if( obj == NULL ) { throw("invalid memory object passed to defer"); } grab(); }
// its error-prone to automatically release on destruction, better to manually do it
~deferred() {}

// adds or removes a reference to the allocated memory
int grab() { return ++m_refcount; }
int drop() { if( --m_refcount == 0 ){ release(); } return m_refcount; }

// seamlessly cast the memory pointer back to its native type
operator T() { if( m_managed == NULL ) { throw("invalid memory object inside defer"); } return *m_managed; }

T& operator =(T B) { if( m_managed == NULL ) { throw("invalid memory object inside defer");} *m_managed = B; return *m_managed; }

// in case implicit typecasting doesn't work, here's an explicit method to cast the pointer
T& p() { if( m_managed == NULL ) throw("invalid memory object inside explicit defer typecast"); return *m_managed; }
};


/***
  * forward declare the string object from standard lib
  * so we can keep it out of our header
  *
  **/

#include <string>

typedef unsigned int u32;
typedef   signed int s32;

#ifdef WITH_UNIT_TESTS

/**
 * forward declare our unit testing group so we may reference
 * it (keep it seperate from actual production code)
 *
 **/

 bool Dequeue_UnitTests();

#endif

template<class T>
class Deque {
private:

#ifdef WITH_UNIT_TESTS

  // allow our unit testing function access to private properties
  friend bool Deque_UnitTests();

#endif

  // size of the current pool, in elements
  u32     m_capacity;

  // # of elements in pool
  u32     m_size;

  // pointer to allocated heap memory pool
  T*   m_pool;

  // faster to keep pointers rather than indices
  T*   m_front, *m_back;

protected:

  // grows the queue's capacity to the requested size, zeroing any elements
  //  @retval : the new capacity of the queue, or 0 if failed
  int grow(int newCap) {
    if( newCap < 8 ) newCap = 8;

    if( newCap < m_capacity ) return m_capacity;

    T* newPool = new T[newCap], *capend = m_pool + (m_capacity*sizeof(T));
    T* scratch = newPool;

    for (int i = 0; i < m_size; i++, m_front++, scratch++ ) {
      if( m_front >= capend ) {
        m_front = m_pool;
      }

      *scratch = *m_front;
    }

    m_back = m_pool;
    m_pool = newPool;

    if( m_back )
      delete m_back;

    m_front = m_pool;
    m_back  = m_pool + m_size * sizeof(T);
    m_capacity = newCap;

    return m_capacity;
  }

  // shrinks the queue's capacity to the requested size, clipping any elements
  // that may get in the way
  //  @retval : the new capacity/size of the queue, or 0 if failed
  int  shrink(int newCap) {
    return m_capacity;
  }

public:

  // initialize our internal variables
  Deque(): m_capacity(0), m_size(0), m_pool(NULL), m_front(NULL), m_back(NULL) { grow(8); }

  // sets an initial capacity for the Q (size is still 0)
  Deque(int cap): m_capacity(0), m_size(0), m_pool(NULL), m_front(NULL), m_back(NULL)  { grow(cap); }

  // releases references to managed memory
  ~Deque() {
    if( m_pool ) {
      delete m_pool;
    }

    // set our pointers to a unique value so we can recognize
    // a 'use-after-free' error
    m_pool = m_front = m_back = (T*) 0xDEADC0DE;

    m_size = m_capacity = 0;
  }

  // pushes a copy* of the item into the queue
  //  @retval: the new element count in Q, or 0 if an error occurred
  int push_front(T item) {
    if( m_size >= m_capacity ) {
      grow(m_capacity*2);
    }

    *m_front = item;
    m_front --;

    if( m_front < m_pool ) {
      m_front = m_pool + (m_capacity-1) * sizeof(T);
    }

    return ++m_size;
  }

  int push_back(T item)  {
    if( m_size >= m_capacity ) {
      grow(m_capacity*2);
    }

    *m_back = item;
    m_back ++;

    if( m_back >= (m_pool + (m_capacity * sizeof(T))) ) {
      m_back = m_pool;
    }

    return ++m_size;
  }

  // returns a copy of the item originally pushed into the queue
  T pop_front() {
    if( m_size == 0 ) {
      #ifdef DEBUG
      throw( "attempting to Deque::pop_front an empty queue" );
      #endif
      return T();
    }

    m_front ++;

    if( m_front >= (m_pool + (m_capacity * sizeof(T))) ) {
      m_front = m_pool;
    }

    m_size --;

    return *m_front;
  }

  T pop_back() {
    if( m_size == 0 ) {
      #ifdef DEBUG
      throw( "attempting to Deque::pop_back an empty queue" );
      #endif
      return T();
    }

    m_back --;

    if( m_back < m_pool ) {
      m_back = m_pool + (m_capacity-1) * sizeof(T);
    }

    m_size --;

    return *m_back;
  }

  // allows a peek at the front and rear elements without popping anything
  T peek_front() {
    if( m_size == 0 ) {
      return T();
    }

    return *m_front;
  }

  T peek_back() {
    if( m_size == 0 ) {
      return T();
    }

    return *m_back;
  }

  // returns an element from the queue (referenced from front of queue)
  T get(int index) {
    if( index >= m_size || index < 0 ) {
      #ifdef DEBUG
      throw( "attempting to access Dequeue::get index that is out of bounds" );
      #endif
      return T();
    }

    T* pool_end = m_pool + m_capacity*sizeof(T);
    T* which = m_front + index * sizeof(T);

    if( which > pool_end ) {
      which = which - pool_end + m_pool;
    }

    return *which;
  }

  // returns the number of elements in the queue
  //  @retval: current # of elements in Q
  int size() {
    return m_size;
  }

  // the current capacity of the queue, in elements
  //  @retval: current Q capacity, in elements
  int capacity() { return m_capacity; }

  //  @retval : true if the queue holds no elements
  bool empty() { return (m_size == 0); }

  // empties the queue but doesn't change capacity
  //  @retval : the current capacity of the queue (unchanged)
  int  clear() {
    m_front = m_back = m_pool;

    m_size = 0;

    return m_capacity;
  }

  // returns a memory managed string representing the current Q state
  std::string toStr() {

    if( m_size == 0 ) {
      return std::string("The Queue is Empty\n");
    }

    T* which = m_back;
    std::string output = "";

    for (int index = 0; index < m_size; index ++, which --) {
      if( which < m_pool ) {
        which = m_pool + (m_capacity-1) * sizeof(T);
      }

      output += std::string(*which) + "\n";
    }

    return output;
  }
};



#endif
