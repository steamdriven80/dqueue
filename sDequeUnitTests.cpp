
#ifdef WITH_UNIT_TESTS

#include "sDeque.h"
#include "recheck.h"

bool Deque_UnitTests() {


    stack("Deque Unit Tests");

    it("should allocate an initial capacity");

      Deque<int> Q;

    expect(Q.m_pool).toNotBeNull();


     it("should allocate the default of 8 elements capacity");

       Deque<int> Q;

     expect(Q.m_capacity).toBeEqual(8);


     it("should allocate the requested capacity");

       Deque<int> Q(16);

     expect(Q.m_capacity).toBeEqual(16);


    it("shouldn't have any initial elements");

      Deque<int> Q;

    expect(Q.m_size).toBeEqual(0);


    it("should have equal front and back pointers");

      Deque<int> Q;

    expect(Q.m_front).toBeEqual(Q.m_back);


    it("should push an object to the back of the queue");

      Deque<int> Q;

      Q.push_back(5);

    expect(*(Q.m_back-1)).toBeEqual(5);


    it("should push an object to the front of the queue");

      Deque<int> Q;

      Q.push_front(3);

    expect(*(Q.m_pool)).toBeEqual(3);


    it("should pop an item from the back of the queue");

      Deque<int> Q;

      Q.push_back(9);

    expect(Q.pop_back()).toBeEqual(9);
  

    it("should pop an item from the front of the queue");

      Deque<int> Q;

      Q.push_front(22);

    expect(Q.pop_front()).toBeEqual(22);
  //
  //
  //   it("should resize the queue when capacity is exceeded");
  //
  //     Deque<int> Q;
  //
  //     for (u32 i = 0; i < Q.m_capacity+1; i ++) {
  //       Q.push_back(i);
  //     }
  //
  //   expect(Q.m_capacity).toBeEqual(16);
  //
  //
  //   it("should shrink capacity when size drops below 1/4 max");
  //
  //     Deque<int> Q;
  //
  //     // add 20 elements to queue
  //     for (int i = 0; i < 20; i++) {
  //       Q.push_back(i);
  //     }
  //
  //     // capacity should be 32
  //
  //     // pop 13 elements and size should == 7
  //     for (int i = 0; i < 13; i++ ) {
  //       Q.pop_back();
  //     }
  //     // since 7 < 32/4, the Q should have resized to a capacity of 16
  //
  //     expect(Q.m_capacity).toBeEqual(16);
  // }

  return true;
}

int main() {
  Deque_UnitTests();

  return 1;
}

#endif
